Write-Output("@NeonUserBot Windows Auto-Deploy")
Write-Output("Scoop Yüklenir...")
Set-ExecutionPolicy RemoteSigned -scope CurrentUser
Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://get.scoop.sh')
Write-Output("Python & Git Yüklenir...")
scoop install git python@3.8

$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")
Write-Output("Installer Yüklenir...")

git clone https://github.com/TheOksigen/installer
Set-Location installer
python3.8 -m pip install -r requirements.txt
python3.8 -m neon_installer
